#!/usr/bin/speedy -w -- -t100 -M1
# -*- perl -*-
# Main CGI program's logic; must be run inside a lock.

# rrd-graphs/cgi - part of rrd-graphs, a tool for online graphs
# Copyright 2010, 2012 Ian Jackson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


use strict qw(vars);
use CGI::SpeedyCGI qw/:standard -no_xhtml/;
use CGI qw/:standard -no_xhtml/;
use POSIX;
use MD5;

sub fail ($) {
    print(header(-status=>500),
          start_html('Error'),
          h1('Error'),
          escapeHTML($_[0]),
          end_html());
    exit 0;
}

our (@sections, %section_groups, %group_elems, %graphs);

#---------- initialisation code, run once - graphs setup ----------

BEGIN {

our $R= '/var/lib/collectd/rrd/chiark.greenend.org.uk';
our $SELF= '/home/ijackson/things/rrd-graphs';

our @timeranges= (3600, map { $_*86400 } qw(1 7 28), 13*7+1, 366);

sub graph_of_group ($$$$$;$) {
    my ($section, $group, $elem, $basis, $args, $title) = @_;
    $basis->{Args}= $args;
    $basis->{Slower}= 0 unless exists $basis->{Slower};
    $basis->{TimeRanges} ||= \@timeranges;
    if (!defined $title) {
	$title = $group;
	if (length $elem) { $title.= " $elem"; }
    }
    $basis->{Title} = $title;
    $graphs{$section,$group,$elem}= $basis;
    if (!exists $group_elems{$section,$group}) {
	# new group then
	if (!exists $section_groups{$section}) {
	    # new section even
	    push @sections, $section;
	}
	push @{ $section_groups{$section} }, $group;
    }
    push @{ $group_elems{$section,$group} }, $elem;
}

sub graph ($$$$) {
    my ($section, $gname, $basis, $args) = @_;
    graph_of_group($section, $gname,'', $basis, $args);
}

graph('General', 'Load and processes', { },
      [
       "DEF:load=$R/load/load.rrd:shortterm:AVERAGE",
       (map { "DEF:$_=$R/processes/ps_state-$_.rrd:value:AVERAGE" }
	    qw(blocked running stopped paging sleeping zombies)),
       "AREA:running#88f:running:STACK",
       "AREA:blocked#8f8:disk wait:STACK",
       "AREA:paging#f88:paging:STACK",
       "LINE:load#000:load",
       ]);

graph('General', 'Processes', { },
      [
       (map { "DEF:$_=$R/processes/ps_state-$_.rrd:value:AVERAGE" }
	    qw(blocked running stopped paging sleeping zombies)),
       "CDEF:busy=0".(join '', map { ",$_,+" } qw(running blocked paging)),
       "AREA:sleeping#ccc:sleeping:STACK",
       "AREA:stopped#00f:stopped:STACK",
       "AREA:zombies#ff0:zombie:STACK",
       "AREA:busy#000:busy:STACK",
       ]);

graph('General', 'CPU', { Units => '[%]' },
      [
       (map {
	   my $thing= $_;
	   (map { "DEF:$thing$_=$R/cpu-$_/cpu-$thing.rrd:value:AVERAGE" }
	        (0..7)),
	   "CDEF:$thing=0".join('', map { ",$thing$_,+" } (0..7)).",8.0,/";
       } qw(idle interrupt nice softirq steal system user wait)),
       "CDEF:allintr=softirq,steal,+,interrupt,+",
       "AREA:allintr#ff0:interrupt:STACK",
       "AREA:system#88f:system:STACK",
       "AREA:user#00f:user:STACK",
       "AREA:nice#ccc:nice:STACK",
       "AREA:wait#f00:wait:STACK",
       ]);

graph('General', 'Memory', { },
      [ '-b',1024,
       (map { "DEF:swap_$_=$R/swap/swap-$_.rrd:value:AVERAGE" }
	    qw(free used cached)),
       (map { "DEF:mem_$_=$R/memory/memory-$_.rrd:value:AVERAGE" }
	    qw(buffered free used cached)),
       "CDEF:c_swap_used=0,swap_used,-",
       "CDEF:c_swap_cached=0,swap_cached,-",
       "CDEF:c_swap_free=0,swap_free,-",
       "AREA:c_swap_used#000:used swap",
       "AREA:c_swap_cached#888:\"cached\" swap:STACK",
#       "AREA:c_swap_free#88f:free swap:STACK",
       "AREA:mem_used#ff0:used memory",
       "AREA:mem_buffered#00f:page cache:STACK",
       "AREA:mem_cached#008:buffer cache:STACK",
       "AREA:mem_free#ccc:unused memory:STACK",
       ]);

graph('General', 'Network', { Units => '[/sec; tx +ve; errs x1000]' },
      [
       (map {
	   ("DEF:tx_$_=$R/interface/if_$_-eth0.rrd:tx:AVERAGE",
	    "DEF:rx_$_=$R/interface/if_$_-eth0.rrd:rx:AVERAGE",
	    "CDEF:mrx_$_=0,rx_$_,-")
	   } qw(octets packets errors)),
       (map {
	   ("CDEF:${_}_kb=${_}_octets,1024,/",
	    "CDEF:${_}_errsx=${_}_errors,1000,*")
	   } qw(mrx tx)),
       "AREA:tx_kb#080:kby",
       "LINE:tx_packets#0f0:pkts",
       "LINE:tx_errsx#000:errs",
       "AREA:mrx_kb#008:kby",
       "LINE:mrx_packets#00f:pkts",
       "LINE:mrx_errsx#444:errs",
      ]);

graph('General', 'Users', {  },
      [
       "DEF:users=$R/users/users.rrd:users:AVERAGE",
       "LINE:users#008:users"
       ]);

foreach my $src (<$R/df/df-*.rrd>) {
    my $vol= $src;
    $vol =~ s,\.rrd$,, or next;
    $vol =~ s,.*/,,;
    $vol =~ s,^df-,,;
    graph('Disk space', $vol, {
	     Slower => 1,
          },
	  [ '-A','-l',0,'-r',
	   qw(-b 1024 -l 0),
	   (map { "DEF:$_=$src:$_:AVERAGE" } qw(free used)),
	   "AREA:used#000:used:STACK",
	   "AREA:free#88f:free:STACK",
	   ]);
}

our %news_name_map;

if (!open NM, '<', "$SELF/data/news/name-map") {
    die unless $!==&ENOENT;
} else {
    while (<NM>) {
	s/^\s*//; s/\s+$//;
	next unless m/^[^\#]/;
	m/^(\S+)\s+(in|out|\*)\s+(\S+)$/ or die;
	if ($2 eq '*') {
	    $news_name_map{$1,$_}= $3 foreach qw(in out);
	} else {
	    $news_name_map{$1,$2}= $3;
	}
    }
}

our %news_sources;

foreach my $src (<$SELF/data/news/*.rrd>) {
    my $site= $src;
    $site =~ s,\.rrd$,, or next;
    $site =~ s,.*/,,;
    $site =~ s,_(in|out)$,,;
    my $inout= $1;
    $site =~ s/^([-.0-9a-z]+)_//;
    my $us= $1; # all very well but we ignore it
    my $newsite= $news_name_map{$site,$inout};
    $site= $newsite if defined $newsite;
    next if $site eq '-';
    push @{ $news_sources{$site}{$inout} }, $src;
}

our @news_graphs;

foreach my $site (keys %news_sources) {
    my $sk= $site;
    $sk =~ s/^[.0-9]+$/~$&/;
    for (;;) {
        last unless $sk =~
 s/^[^. ]*\b(?:chiark|greenend|news|newsfeed|nntp|peer|feeds?|feeder|in|out)\b[^.]*\.//;
        $sk .= " $&";
    }
print STDERR "$site => $sk\n";
    foreach my $inout (keys %{ $news_sources{$site} }) {
	my $skio = $sk;
	$skio =~ s/ / [$inout]/;
        push @news_graphs, [ $skio, $site, $inout ];
    }
}

foreach my $siteinfo (sort { $a->[0] cmp $b->[0] } @news_graphs) {
    my ($sortkey, $site, $inout)= @$siteinfo;
    my @sources= @{ $news_sources{$site}{$inout} };

    my @vals= $inout eq 'out'
        ? qw(missing deferred unwanted accepted rejected body_missing)
        : qw(accepted refused rejected duplicate
             accepted_size duplicate_size);
    my @defs;
    foreach my $val (@vals) {
        my $def= "CDEF:$val=0";
        foreach my $si (0..$#sources) {
            my $src= $sources[$si];
            my $tvar= "${val}_${si}";
            push @defs, "DEF:$tvar=$src:$val:AVERAGE";
            $def .= ",$tvar,ADDNAN";
        }
        push @defs, $def;
        if ($val =~ m/_size$/) {
            push @defs, "CDEF:kb_$`=$val,1024,/";
        }
    }
    my $group = $sortkey;
    $group =~ s/ .*//;
    $group = $site unless length $group;
    graph_of_group("News", $group, "$site $inout",
	  {
		Units => '[art/s]',
		TimeRanges => [ map { $_*86400 } qw(1 7 31), 366, 366*3 ]
	    }, $inout eq 'out' ?
	  [
	   @defs,
	   "AREA:accepted#00f:ok",
	   "AREA:body_missing#ff0:miss:STACK",
	   "AREA:rejected#f00:rej:STACK",
	   "AREA:unwanted#aaa:unw:STACK",
	   "AREA:deferred#ddd:defer:STACK",
	   ] :
	  [
	   @defs,
	   "AREA:accepted#00f:ok:STACK",
	   "AREA:rejected#f00:rej:STACK",
	   "AREA:duplicate#000:dupe:STACK",
	   "AREA:refused#aaa:unw:STACK",
	   "CDEF:kb_accepted_smooth=kb_accepted,<interval/60>,TREND",
	   "LINE:kb_duplicate#ff0:kb dupe",
	   "LINE:kb_accepted_smooth#008:~kb",
	   ],
	"$site $inout");
}

our %disk_rdev2rrd;

foreach my $physdiskrrd (<$R/disk-*/disk_octets.rrd>) {
    $physdiskrrd =~ s,octets\.rrd$,, or die;
    $physdiskrrd =~ m,-([^/]+)/disk_$, or die;
    my $physdev= "/dev/$1";
    if (!stat $physdev) {
        die "$physdev $!" unless $!==&ENOENT;
        next;
    }
    die "$physdev ?" unless S_ISBLK((stat _)[2]);
    $disk_rdev2rrd{(stat _)[6]}= $physdiskrrd;
}

our @disk_vgs;

sub lvgraphs {
    my ($vg, $label, $factor, $rcolour, $wcolour) = @_;
    my @lvs;
    my $varname= $vg;
    $varname =~ s/[^0-9a-zA-Y]/ sprintf "Z%02x", ord($&) /ge;
    my $vginfo= {
        Name => $label,
        Varname => $varname,
        Colour => { 'read' => $rcolour, 'write' => $wcolour },
        Lvs => []
    };
    foreach my $bo (qw(octets ops)) {
        foreach my $rw (qw(read write)) {
            $vginfo->{VarDefs}{$bo}{$rw}= [];
            $vginfo->{Sumdef}{$bo}{$rw}= '0';
        }
    }
    my $ix=0;
    foreach my $lvpath (</dev/$vg/*>) {
        my $lv= $lvpath; $lv =~ s,.*/,,;
        if (!stat $lvpath) {
            die "$lvpath $!" unless $!==&ENOENT;
            next;
        }
        die "$lvpath ?" unless S_ISBLK((stat _)[2]);
        my $rrd= $disk_rdev2rrd{(stat _)[6]};
        next unless defined $rrd;

        my $lvinfo= { Name => $lv };
        push @{ $vginfo->{Lvs} }, $lvinfo;

        foreach my $bo (qw(octets ops)) {
            $lvinfo->{Defs}{$bo}=
              [
               (map { ("DEF:$_=${rrd}${bo}.rrd:$_:AVERAGE") } qw(read write)),
               "CDEF:mwrite=0,write,-",
               "AREA:read#00f:read",
               "AREA:mwrite#f00:write"
               ];

            foreach my $rw (qw(read write)) {
                $ix++;
                my $tvar= "lv_${rw}_${bo}_${varname}_${ix}";
                push @{ $vginfo->{VarDefs}{$bo}{$rw} },
                    "DEF:$tvar=${rrd}${bo}.rrd:$rw:AVERAGE";
                $vginfo->{Sumdef}{$bo}{$rw} .= ",$tvar,+";
            }
        }
    }
    foreach my $bo (qw(octets ops)) {
        foreach my $rw (qw(read write)) {
            my $defs= [];
            push @$defs, @{ $vginfo->{VarDefs}{$bo}{$rw} };
            push @$defs, "CDEF:${rw}_vg_${varname}=".
                $vginfo->{Sumdef}{$bo}{$rw}.
                sprintf(",%f,*", $rw eq 'write' ? -$factor : $factor);
            $vginfo->{Defs}{$bo}{$rw}= $defs;
        }
    }
    push @disk_vgs, $vginfo;
}

lvgraphs('vg-main',          'main',     1, qw(00f f00));
lvgraphs('vg-chiark-stripe', 'stripe', 0.5, qw(008 800));

foreach my $bo (qw(octets ops)) {
    my @a= ();
    foreach my $rw (qw(read write)) {
        my $stack= '';
        foreach my $vginfo (@disk_vgs) {
            push @a, @{ $vginfo->{Defs}{$bo}{$rw} };
            push @a, "AREA:${rw}_vg_$vginfo->{Varname}#".
                $vginfo->{Colour}{$rw}.
                ":$vginfo->{Name} ".substr($rw,0,1).
                $stack;
            $stack= ':STACK';
        }
    }
    graph_of_group('IO', 'IO', $bo, { Units => '[/s]' }, \@a);
}

foreach my $vginfo (@disk_vgs) {
    foreach my $bo (qw(octets ops)) {
        foreach my $lv (@{ $vginfo->{Lvs} }) {
            graph_of_group('IO', "$vginfo->{Name} $lv->{Name}",
                           $bo, { Units => '[/s]' }, $lv->{Defs}{$bo});
        }
    }
}

push @{ $section_groups{General} }, {
    Section => 'IO',
    Group => 'IO',
    UrlParams => "section=IO&sloth=SLOTH"
};

}
#---------- right, that was the initialisation ----------

our $self= url(-relative=>1);

if (param('debug')) {
    print "Content-Type: text/plain\n\n";
}

sub source_tarball ($$) {
    my ($spitoutfn) = @_;
}

if (path_info() =~ m/\.tar\.gz$/) {
    print "Content-Type: application/octet-stream\n\n";

    exec '/bin/sh','-c','
		(
		 git-ls-files -z;
		 git-ls-files -z --others --exclude-from=.gitignore;
		 if test -d .git; then find .git -print0; fi
		) | (
		 cpio -Hustar -o --quiet -0 -R 1000:1000 || \
		 cpio -Hustar -o --quiet -0
		) | gzip
	';
    die $!;
}

our @navsettings;

@navsettings= ();

sub navsetting ($) {
    my ($nav) = @_;
    my $var= $nav->{Variable};
    $$var= param($nav->{Param});
    $$var= $nav->{Default} if !defined $$var;
    die $nav->{Param} unless grep { $_ eq $$var } @{ $nav->{Values} };
    push @navsettings, $nav;
}

our $section;

navsetting({
    Desc => 'Section',
    Param => 'section',
    Variable => \$section,
    Default => $sections[0],
    Values => [@sections],
    Show => sub { return $_[0]; }
});


sub num_param ($$$$) {
    my ($param,$def,$min,$max) = @_;
    my $v= param($param);
    return $def if !defined $v;
    $v =~ m/^([1-9]\d{0,8})$/ or die;
    $v= $1;
    die unless $v >= $min && $v <= $max;
    return $v + 0;
}

our $group= param('graph');

my $defwidth= 370;
my $defheight= 200;

our $elem= param('elem');
if (defined $elem) {
    my $g= $graphs{$section,$group,$elem};
    die unless $g;

    my $width= num_param('w',$defwidth,100,1600);
    my $height= num_param('h',$defheight,100,1600);

    my $sloth= param('sloth');
    die unless defined $sloth;
    $sloth =~ m/^(\d+)$/ or die;
    $sloth= $1+0;
    my $end= $g->{TimeRanges}[$sloth];
    die unless defined $end;

    my @args= @{ $g->{Args} };
    s,\<interval/(\d+)\>, $end/$1 ,ge foreach @args;
    unshift @args, qw(--end now --start), "end-${end}s";
    
    my $title = $g->{Title};
    $title .= " $g->{Units}" if $g->{Units};
    unshift @args, '-t', $title, '-w',$width, '-h',$height;
    unshift @args, qw(-a PNG --full-size-mode);

    my $cacheid= "$section!$group!$elem!$sloth!$width!$height!";
    $cacheid .= unpack "H*", MD5->hash(join '\0', @args);
    my $cachepath= "cache/$cacheid.png";

    if (param('debug')) {
        print((join "\n",@args),"\n"); exit 0;
    }

#print STDERR "||| ",(join ' ', map { "'$_'" } @args)." |||\n";
    exec(qw(sh -ec), <<'END', 'x', $cachepath, @args);
        p="$1"; shift
        rrdtool graph "$p" --lazy "$@" >/dev/null
	printf "Content-Type: image/png\n\n"
	exec cat "$p"
END
    die $!;
}

sub start_page ($) {
    my ($title) = @_;
    print header(), start_html($title);
    my $outerdelim= '';
    foreach my $nav (@navsettings) {
	print $outerdelim;
	print $nav->{Desc}, ": ";
	my $delim= '';
	my $current= $nav->{Variable};  $current= $$current;
	foreach my $couldbe (@{ $nav->{Values} }) {
	    print $delim;
	    my $show= $nav->{Show}($couldbe);
	    if ($couldbe eq $current) {
		print "<b>$show</b>";
	    } else {
		my $u= $self;
		my $delim2= '?';
		foreach my $nav2 (@navsettings) {
		    my $current2= $nav2->{Variable};  $current2= $$current2;
		    $current2= $couldbe if $nav2->{Param} eq $nav->{Param};
		    next if $current2 eq $nav2->{Default};
		    $u .= $delim2;  $u .= "$nav2->{Param}=$current2";
		    $delim2= '&';
	        }
		print a({href=>$u}, $show);
	    }
	    $delim= ' | ';
	}
	$outerdelim= "<br>\n";
    }
    print "\n";

    print h1("$title");
}

our $detail= param('detail');
if (defined $detail) {
    my $elems= $group_elems{$section,$detail};
    die unless $elems;
    start_page("$detail - $section - graphs");
    foreach my $tsloth (0..5) {
	foreach my $elem (@$elems) {
	    my $g= $graphs{$section,$detail,$elem};
	    die unless $g;
	    next if $tsloth >= @{ $g->{TimeRanges} };
	    my $imgurl= "$self?graph=$detail&section=$section".
		"&sloth=$tsloth&elem=$elem";
	    print a({href=>"$imgurl&w=780&h=800"},
		    img({src=>$imgurl, alt=>'',
			 width=>$defwidth, height=>$defheight}));
	}
    }
    print end_html();
    exit 0;
}

our $sloth;

navsetting({
    Desc => 'Time interval',
    Param => 'sloth',
    Variable => \$sloth,
    Default => 1,
    Values => [0..3],
    Show => sub {
	my ($sl) = @_;
	return ('Narrower', 'Normal', 'Wider', 'Extra wide')[$sl];
    }
});

if (param('debug')) {
    use Data::Dumper;
    print Dumper(\%graphs);
    exit 0;
}

start_page("$section - graphs");

foreach my $group (@{ $section_groups{$section} }) {
    my $ref_group= $group;
    my $ref_section= $section;
    my $ref_urlparams= "detail=$group&section=$section";
    if (ref $group) {
        $ref_group= $group->{Group};
        $ref_section= $group->{Section};
        $ref_urlparams= $group->{UrlParams};
        $ref_urlparams =~ s/\bSLOTH\b/$sloth/;
    }
    print a({href=>"$self?$ref_urlparams"});
    my $imgurl= "$self?graph=$ref_group&section=$ref_section";
    print "<span style=\"white-space:nowrap\">";
    my $elems= $group_elems{$ref_section,$ref_group};
    foreach my $elem (@$elems) {
	my $g= $graphs{$ref_section,$ref_group,$elem};
	print img({src=>"$imgurl&elem=$elem&sloth=".($sloth + $g->{Slower}),
	           alt=>'', width=>$defwidth, height=>$defheight});
    }
    print "</span>";
    print "</a>\n";
}

my $homeurl =
    'http://www.chiark.greenend.org.uk/ucgi/~ijackson/git/rrd-graphs/';
print <<END
<hr>Generated by "<a href="$homeurl">rrd-graphs</a>".
Copyright 2010,2012 Ian Jackson.  There is <strong>NO WARRANTY</strong>.
Available under the Affero Public General Licence, version 3 or any
later version.  You may download the
<a href="$self/rrd-graphs.tar.gz">source code</a>
for the version currently running here.
END
